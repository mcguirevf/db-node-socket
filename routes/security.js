exports.wrap = function (callback) {
    return function (req, res) {
        if (req.session.user) {
            return callback(req, res);

        } else {
            res.redirect('/');
        }
    }
};

exports.login =  function(pg) {
	return	function (req, res) {

		pg.connect(conString, function(err, client, done) {
  		   if(err) {
			 return console.error('error fetching client from pool', err);
		   }

		   client.query("SELECT * FROM users WHERE user_name = '"+ req.body.loginname + "'", function(err, result) {

				done();

				if(err) {
				   return console.error('error running query', err);
			    }

				if (result.rows.length == 0)
				{
					res.redirect('/');
					return;
				}
				var pw = result.rows[0].user_password;

				if (pw != req.body.loginpassword)
				{
					res.redirect('/');
				} else {

					console.log('loggedin : ' + pw);
					req.session.user = req.body.loginname;
					req.session.user_id = result.rows[0].user_id;

					//res.redirect('/eventlist');			

					res.render('eventlist', {			
						title: 'Event List',
						user_id: req.session.user_id,
						user_name: req.session.user
					});
				}

		   });

		});
		
	};
};

exports.logout = function() {
	return 	function (req, res) {
	    req.session.user = null;
		req.session.user_id = null;

	    res.redirect('/');
	}
}

