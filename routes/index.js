
/*
 * GET home page
 */

exports.index = function(req, res){
  res.render('index', { title: 'Login' });
};


/*
 * GET EventList from DB
 */

exports.eventlist = function(pg) {
	return function(req, res) {

		res.render('eventlist', {			
					title: 'Event List',
					user_id: req.session.user_id,
					user_name: req.session.user
				});
	};

};

