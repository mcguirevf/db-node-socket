$(document).ready(function()
{
	/** login validation */
	var checkLogin = function(){
		var username = $("#username").val();
		var password = $("#password").val();
		if(!username && !password){
			return false;
		}else{
			if(username){
				if(!password){
					return false;
				}else{
					return true;
				}
			}
		}
	}

	var userlogin = function(){
		
		if(checkLogin()){
			var username = document.frmLogin.username.value;
			var password = document.frmLogin.password.value;

			document.getElementById("frmLogin").submit();
		}
	}

	//Additional Validations
	var arrReqLogin = ['username', 'password'];
	var arrTitleLogin=Array('Username','Password')
	var arrReqForgotName = ['username'];

	//added the argument which will be passed from the page.
	window.checkLoginValidations = function(){

		var selObj = null;
		for(var i = 0; i < arrReqLogin.length; i++){
			var obj = document.getElementById(arrReqLogin[i]);
			var objDiv = document.getElementById('div_' + arrReqLogin[i]);
			var objError = document.getElementById("error_" + arrReqLogin[i]);
			if(!obj.value){
				objError.style.display = "";
				objDiv.innerHTML = arrTitleLogin[i] + " is required";
				if(selObj === null){
					selObj = obj;
				}
			}else{
				objError.style.display = "none";
				objDiv.innerHTML = "";
			}

			if(i + 1 == arrReqLogin.length){
				if(selObj !== null){
					selObj.focus();
					return false;
				}
				userlogin();
			}
		}
	}


	self.onload = function (){ $('#username').focus();}

	document.getElementById('username').onkeyup = keyHandler1;
	document.getElementById('password').onkeyup = keyHandler;

	function keyHandler(e){
		if(window.event)
			var KeyID = (window.event) ? event.keyCode : e.keyCode;
		else
			var KeyID= (e.keyCode)? e.keyCode: e.charCode;
			switch(KeyID)   {
	   		case 13:
			{
				document.getElementById('Login').onclick();				
			}
				break;
		}		
	 }
	 
	 //to move the focus to the password text field.
	 function keyHandler1(e){
		if(window.event)
			var KeyID = (window.event) ? event.keyCode : e.keyCode;
		else
			var KeyID= (e.keyCode)? e.keyCode: e.charCode;
			switch(KeyID)   {
	   		case 13:
			{
				if(!document.getElementById('password').value)
					document.getElementById('password').focus();
				else
					document.getElementById('Login').onclick();				
			}
				break;
		}
	 }

});