module = {}; //allows us to reuse code client and server-side

var socket;
var myip;
var eventlisttable = null;

function ip_callback(o) {
   myip = o.host;
}

socket = io.connect(window.location.hostname, {port: 3000});

socket.on("refreshEventlistData", refresh_eventlist);
socket.on("updateEventlist", display_eventlist);

/*******************************************************/
$(document).ready(function()
{
	var user_id = $("#user_id").val();
	
	socket.emit("getEventlist", {user_id: user_id});	

});

//inform server that data is changes on the client side
function refresh() {
	
	var user_id = $("#user_id").val();

	socket.emit("changedEventlistData", {user_id: user_id});	
}


function refresh_eventlist(data) {
	
	alert('Other user has changed data so datable renders new data !');
	
	display_eventlist(data);
}

function display_eventlist(data)
{		
	var aaData = [];

	for (var i=0; i<data.count; i++ )
	{
		var tmp = [];
		tmp.push(data.eventlist[i].forum_id);
		tmp.push(data.eventlist[i].c_date);
		tmp.push(data.eventlist[i].s_date);
		tmp.push(data.eventlist[i].forum_name);
		tmp.push(data.eventlist[i].role);
		tmp.push(data.eventlist[i].role_id);
		tmp.push(data.eventlist[i].forum_status);
		tmp.push(data.eventlist[i].start_dt);
		tmp.push(data.eventlist[i].end_dt);
		tmp.push(data.eventlist[i].is_copy);
		tmp.push(data.eventlist[i].account);

		aaData.push(tmp);
	}


	if(eventlisttable === null)
    {
		eventlisttable = $('#eventlist').dataTable( {
								"aaData": aaData,
								"bSortCellsTop": true,
								"aoColumns": [
									{ "sTitle": "forum_id" },
									{ "sTitle": "create_dt" },
									{ "sTitle": "schedule_dt" },
									{ "sTitle": "forum_name", "sClass": "center" },

									{ "sTitle": "role", "sClass": "center" },
									{ "sTitle": "role_id" },
									{ "sTitle": "forum_status" },
									{ "sTitle": "start_dt" },
									{ "sTitle": "end_dt" },
									{ "sTitle": "is_copy" },
									{ "sTitle": "account" }					
								]
							} );
    }
    else eventlisttable.fnDestroy();
}

