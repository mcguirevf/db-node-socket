
/**
 * Module dependencies.
 */

var express = require('express');
var http = require('http');
var path = require('path');
var pg   = require('pg');

var routes = require('./routes');
var security = require('./routes/security');
 
global.conString = "postgres://dbreeze:dbreeze2014!@173.244.170.162:5432/audiocast";

var app = express();
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.urlencoded());
app.use(express.cookieParser('constricting snakes mix cocktails')); // do not change this!
app.use(express.methodOverride());
app.use(express.session());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}


app.get('/', routes.index);
app.get('/eventlist', security.wrap(routes.eventlist(pg)));

app.post('/login', security.login(pg));
app.get('/logout', security.logout());

server = http.createServer(app);
server.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});


var io = require('socket.io').listen(server);
/*
io.configure(function(){
	io.set('log level', 0);

	io.set('authorization', function(data, callback) {
		if (data.headers.cookie) {
			var cookie = cookieParser.parse(data.headers.cookie);
			//sessionMongoStore.get(cookie['connect.sid'], function(err, session) {
			sessionStore.get(cookie['connect.sid'], function(err, session) {
				if (err || !session) {
					callback('Error', false);
				} else {
					data.session = session;
					callback(null, true);
				}
			});
		} else {
			callback('No cookie', false);
		}
	});
});*/

io.sockets.on('connection', function (socket) {
	
	socket.on('changedEventlistData', function (data) {
		//do something here.

		var query  = "SELECT f.forum_id as forum_id ,"; 
			query += "to_char(f.create_dt, 'mm/dd/yyyy') as c_date,";
			query += "to_char(f.schedule_dt, 'mm/dd/yyyy') as s_date,";
			query += "f.name  as forum_name,";
			query += "rt.name as role,";
			query += "fr.forum_role_id as role_id,";
			query += "fs.name as forum_status,";
			query += "to_char(f.schedule_dt, 'mm/dd/yyyy HH:MI:SS') as start_dt,";
			query += "to_char(f.schedule_end_dt, 'mm/dd/yyyy HH:MI:SS') as end_dt,";
			query += "allow_copy as is_copy,";
			query += "'TBD' as account ";
			query += "FROM   forum f,";
			query += " forum_status fs,  forum_role fr, role_type rt ";
			query += "WHERE  f.forum_status_id = fs.forum_status_id ";
			query += " AND    f.forum_id = fr.forum_id  AND    fs.name != 'PURGED' AND fs.name != 'ARCHIVED' ";
			query += " AND    fr.role_type_id = rt.role_type_id AND    fr.user_id = ";
			
			query +=  data.user_id;

			query += " ORDER BY f.schedule_dt, forum_name, role ";

		pg.connect(conString, function(err, client, done) {
  		   if(err) {
			 return console.error('error fetching client from pool', err);
		   }

		   client.query(query, function(err, result) {
				done();

			    if(err) {
				   return console.error('error running query', err);
			    }

				var data = result.rows;
				
				io.sockets.emit('refreshEventlistData', {eventlist: data, count: result.rows.length });

		   });

		});		
		
	});

	socket.on('getEventlist', function (data) {

		var query  = "SELECT f.forum_id as forum_id ,"; 
			query += "to_char(f.create_dt, 'mm/dd/yyyy') as c_date,";
			query += "to_char(f.schedule_dt, 'mm/dd/yyyy') as s_date,";
			query += "f.name  as forum_name,";
			query += "rt.name as role,";
			query += "fr.forum_role_id as role_id,";
			query += "fs.name as forum_status,";
			query += "to_char(f.schedule_dt, 'mm/dd/yyyy HH:MI:SS') as start_dt,";
			query += "to_char(f.schedule_end_dt, 'mm/dd/yyyy HH:MI:SS') as end_dt,";
			query += "allow_copy as is_copy,";
			query += "'TBD' as account ";
			query += "FROM   forum f,";
			query += " forum_status fs,  forum_role fr, role_type rt ";
			query += "WHERE  f.forum_status_id = fs.forum_status_id ";
			query += " AND    f.forum_id = fr.forum_id  AND    fs.name != 'PURGED' AND fs.name != 'ARCHIVED' ";
			query += " AND    fr.role_type_id = rt.role_type_id AND    fr.user_id = ";
			
			query +=  data.user_id;

			query += " ORDER BY f.schedule_dt, forum_name, role ";

		pg.connect(conString, function(err, client, done) {
  		   if(err) {
			 return console.error('error fetching client from pool', err);
		   }

		   client.query(query, function(err, result) {
				done();

			    if(err) {
				   return console.error('error running query', err);
			    }

				var data = result.rows;
				
				socket.emit('updateEventlist', {eventlist: data, count: result.rows.length } );

		   });

		});

	});

/*	socket.on('get-players-by-page', function (data) {
		var page	 = data.page;
		var collection = db.get('playercollection');

		collection.find({},{'limit':9, 'skip':page*9, 'sort': {'vote':-1 }},function(e,docs){			
			socket.emit('updateplayerlist', {pagenum:page, playerlist: docs} );
		});		
	});

	socket.on('update-vote', function (data) {
			
		//{playername: player, vote: vote, ip: myip, today:today}

		var playercollection = db.get('playercollection');
		var usercollection = db.get('usercollection');

		usercollection.find({"ip": data.ip, "votedate": data.today, "player":data.playername},{},function(e,docs){	
					
			if (docs.length == 0)
			{
				usercollection.insert({
					"ip" : data.ip,
					"votedate" : data.today,
					"player": data.playername
				});

				playercollection.find({"name": data.playername},{},function(e,docs){	
					playercollection.update({ "name": data.playername }, { "name": data.playername, "url": docs[0].url, "vote": data.vote+1 });
				});

				console.log("new ip : "+data.ip + "," + data.today);
				socket.emit('checkedvote', {'name': data.playername,'updated': 1}) ;				
			} else {
				console.log("new ip : "+docs[0].ip + "," + docs[0].votedate + "," + docs[0].player);
				socket.emit('checkedvote', {'name': data.playername,'updated': 0}) ;		
			}
		});

	});*/
});

